#ifndef _THREADS_H_
#define _THREADS_H_

#include <pthread.h>
#include <errno.h>
#include <semaphore.h>



void pTHREAD_CREATE( pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg );
void pTHREAD_JOIN( pthread_t thread, void **retval );
void pTHREAD_MUTEX_INIT( pthread_mutex_t *mutex, const pthread_mutexattr_t *mutexattr );
void pTHREAD_MUTEX_DESTROY( pthread_mutex_t *mutex );
void pTHREAD_MUTEX_LOCK( pthread_mutex_t *mutex );
void pTHREAD_MUTEX_UNLOCK( pthread_mutex_t *mutex );
int pTHREAD_MUTEX_TRYLOCK( pthread_mutex_t *mutex );
void pTHREAD_COND_INIT( pthread_cond_t *cond, pthread_condattr_t *cond_attr );
void pTHREAD_COND_DESTROY( pthread_cond_t *cond );
void pTHREAD_COND_WAIT( pthread_cond_t *cond, pthread_mutex_t *mutex );
void pTHREAD_COND_SIGNAL( pthread_cond_t *cond );
void pTHREAD_COND_BROADCAST( pthread_cond_t *cond );
void pTHREAD_BARRIER_INIT( pthread_barrier_t *restrict barrier, const pthread_barrierattr_t *restrict attr, unsigned count );
void pTHREAD_BARRIER_DESTROY( pthread_barrier_t *barrier );
void pTHREAD_BARRIER_WAIT( pthread_barrier_t *barrier );
void pTHREAD_DETACH( pthread_t thread );
void pTHREAD_ATTR_INIT( pthread_attr_t *attr );
void pTHREAD_ATTR_DESTROY( pthread_attr_t *attr );
void pTHREAD_ATTR_SETDETACHSTATE( pthread_attr_t *attr, int detachstate );
void pTHREAD_MUTEXATTR_SETTYPE( pthread_mutexattr_t *attr, int kind );
void sem_INIT( sem_t *sem, int pshared, unsigned int value );
void sem_DESTROY( sem_t *sem );
void sem_WAIT( sem_t *sem );
void sem_POST( sem_t *sem );
void sem_GETVALUE( sem_t *sem, int *sval );


#endif
