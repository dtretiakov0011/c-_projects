#include "storage.h"
#include "threads.h"
#include "storage-pass.h"

pthread_t menu_th;

pthread_mutex_t main_mutex;
pthread_cond_t condMenu;
sem_t semMenu;


static void init_threads( void );
static void * menu_thread( void * arg );
static void new_storage( void );
static void open_storage( void );
static void menu_print( void );

int main()
{


init_threads();

}


static void init_threads( void )
{
    sem_INIT( &semMenu, 0, 1 );
    //pTHREAD_COND_INIT( &condMenu, NULL );
    //pTHREAD_MUTEX_INIT( &main_mutex, NULL );
    pTHREAD_CREATE( &menu_th, NULL, &menu_thread, NULL );


    pTHREAD_JOIN( menu_th, NULL );
    //pTHREAD_MUTEX_DESTROY( &main_mutex );
}

static void * menu_thread( void * arg )
{
    while ( 1 )
    {
       sem_WAIT( &semMenu );

        menu_print();

        
    }
   
}

static void menu_print( void )
{
    while( 1 )
    {
        int i = 0;

        printf( "(1) New storage\n(2) Open storage\n" );
        
        scanf( "%d", &i );

        if( i == 1 )
            new_storage();
        else if( i == 2 )
            open_storage();
        else
            printf( "invalid!%d\n", i );
    }
}

// can be overflow!!!!!
static void new_storage( void )
{
    char new_login[ 10 ];
    char new_pass[ 20 ];

    printf( "Enter login: " );
    scanf( "%s", new_login );

    printf( "Enter pass: " );
    scanf( "%s", new_pass );

    add_new_user( new_login, new_pass );
}

// can be overflow!!!!!
static void open_storage( void )
{
    char login[ 10 ];
    char pass[ 20 ];

    printf( "Enter login: " );
    scanf( "%s", login );

    printf( "Enter pass: " );
    scanf( "%s", pass );

    check_user( login, pass );
}




