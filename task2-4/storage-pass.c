#include "storage-pass.h"

static int open_add_users_file( void );
static int open_check_users_file( void );
static int read_str( int fd, char * str );
static int check_login( char * login, char * str );
static int check_pass( char * pass, char * str );


static int open_add_users_file( void )
{
    int fd;

    if( ( fd = open( "user-file.txt", O_APPEND | O_RDWR | O_CREAT, S_IRUSR | S_IWUSR ) ) == -1 )
    {
        perror( "open_users_file errror: " );
        exit( EXIT_FAILURE );
    } 
    return fd;
}

void add_new_user( char * login, char * pass )
{
    int fd, i;

    fd = open_add_users_file();

    if( ( i = write( fd, login, strlen( login )) ) <= 0  )
    {
        perror( "add_new_user 'login': " );
        exit( EXIT_FAILURE );
    }
    write( fd, " ", 1 );
    if( ( i = write( fd, pass, strlen( pass )) ) <= 0  )
    {
        perror( "add_new_user: 'pass'" );
        exit( EXIT_FAILURE );
    }
    write( fd, "\n", 1 );
    
    close( fd );
}

void check_user( char * login, char * pass )
{
    int fd, ret;
    char str_buff[ 35 ], log_buff[ 10 ], pass_buff[ 20 ], ch = '2';


    fd =  open_check_users_file();

    while( 1 )
    {
    //read str
        if( !read_str( fd, str_buff ) )
        {
            printf( "end of file!!\n" );
            exit( 0 );
        }

    //check login
        if( check_login( login, str_buff ) )
        {//check pass
            if( check_pass( pass, str_buff ) )
            {
                //Good!!!
                printf(" GOOD!!\n ");
                exit( 0 );
            }
            else
                continue;
        }
    }




 

}

static int open_check_users_file( void )
{
    int fd;

    if( ( fd = open( "user-file.txt", O_RDONLY ) ) == -1 )
    {
        perror( "open_check_users_file no users already: " );
        exit( EXIT_FAILURE );
    } 
    return fd;
}

static int read_str( int fd, char * str )
{
    int ret, i = 0;
    char ch = '\0';

    while( 1 )
    {
        if(  (ret = read( fd, &ch, 1 )) < 0 )
        {
            perror("check_user: ");
            exit( EXIT_FAILURE );
        }
        else if( ret == 0 )
        {
            return 0;
        }
        else
        {
            if( ch != '\n' )
            {
                str[ i++ ] = ch;
            }
            else
            {
                str[ i ] = '\0';
                return 1;
            }
        }
    }
}

static int check_login( char * login, char * str )
{
    int i = 0;
    char log [ 11 ] = { 0 };

    for( ; ; ++i)
    {
        if( str[ i ] == ' ' )
            break;
        else
            log[ i ] = str[ i ]; 
    }

    if( strlen( log ) == strlen( login ) )
        if( !strcmp( log , login) )
            return 1; // eq
    else    
        return 0;
}

static int check_pass( char * pass, char * str )
{
    int i = 0, ii = 0;
    char pas[ 21 ] = { 0 };

    for( ; ; ++i)
    {
        if( str[ i ] == ' ' )
            break;
    }
    i++;
    for( ; ; ++i )
    {
        if( str[ i ] == '\0' )
            break;
        else
            pas[ ii++ ] = str[ i ];
    }

    if( strlen( pas ) == strlen( pass ) )
        if( !strcmp( pas , pass) )
            return 1; // eq
    else    
        return 0;
}

