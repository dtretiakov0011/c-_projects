#include "threads.h"

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>


void pTHREAD_CREATE( pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg )
{
    errno = 0;
    if( pthread_create( thread, attr, start_routine, arg ) )
    {
        perror("pTHREAD_CREATE() ");
    }
}

void pTHREAD_JOIN( pthread_t thread, void **retval )
{
    errno = 0;
    if( pthread_join( thread, retval ) )
    {
        perror("pTHREAD_JOIN() ");
    }
}

void pTHREAD_MUTEX_INIT( pthread_mutex_t *mutex, const pthread_mutexattr_t *mutexattr )
{
    errno = 0;
    if( pthread_mutex_init( mutex, mutexattr ) )
    {
        perror("pTHREAD_INIT() ");
    }
}

void pTHREAD_MUTEX_DESTROY( pthread_mutex_t *mutex )
{
    errno = 0;
    if( pthread_mutex_destroy( mutex ) )
    {
        perror("pTHREAD_MUTEX_DESTROY() ");
    }
}

void pTHREAD_MUTEX_LOCK( pthread_mutex_t *mutex )
{
    errno = 0;
    if( pthread_mutex_lock( mutex ) )
    {
        perror("pTHREAD_MUTEX_LOCK() ");
    }
}

void pTHREAD_MUTEX_UNLOCK( pthread_mutex_t *mutex )
{
    errno = 0;
    if( pthread_mutex_unlock( mutex ) )
    {
        perror("pTHREAD_MUTEX_UNLOCK() ");
    }
}

int pTHREAD_MUTEX_TRYLOCK( pthread_mutex_t *mutex )
{
    errno = 0;
    int ret;
    if( (ret = pthread_mutex_trylock( mutex )) != 0 )
    {
        perror("pTHREAD_MUTEX_TRYLOCK() EBUSY");
    }
    return ret;
}

void pTHREAD_COND_INIT( pthread_cond_t *cond, pthread_condattr_t *cond_attr )
{
    errno = 0;
    if( pthread_cond_init( cond, cond_attr ) );
    {
        perror("pTHREAD_COND_INIT() ");
    }
}

void pTHREAD_COND_DESTROY( pthread_cond_t *cond )
{
    errno = 0;
    if( pthread_cond_destroy( cond ) );
    {
        perror("pTHREAD_COND_DESTROY() ");
    }
}

void pTHREAD_COND_WAIT( pthread_cond_t *cond, pthread_mutex_t *mutex )
{
    errno = 0;
    if( pthread_cond_wait( cond, mutex ) );
    {
        perror("pTHREAD_COND_WAIT() ");
    }
}

void pTHREAD_COND_SIGNAL( pthread_cond_t *cond )
{
    errno = 0;
    if( pthread_cond_signal( cond ) );
    {
        perror("pTHREAD_COND_SIGNAL() ");
    }
}

void pTHREAD_COND_BROADCAST( pthread_cond_t *cond )
{
    errno = 0;
    if( pthread_cond_broadcast( cond ) );
    {
        perror("pTHREAD_COND_BROADCAST() ");
    }
}

void pTHREAD_BARRIER_INIT( pthread_barrier_t *restrict barrier, const pthread_barrierattr_t *restrict attr, unsigned count )
{
    errno = 0;
    if( pthread_barrier_init( barrier, attr, count ) )
    {
        perror("pTHREAD_BARRIER_INIT() ");
    }
}

void pTHREAD_BARRIER_DESTROY( pthread_barrier_t *barrier )
{
    errno = 0;
    if( pthread_barrier_destroy( barrier ) )
    {
        perror("pTHREAD_BARRIER_DESTROY() ");
    }
}

void pTHREAD_BARRIER_WAIT( pthread_barrier_t *barrier )
{
    errno = 0;
    if( ( pthread_barrier_wait( barrier ) ) > 0 )
    {
        perror("pTHREAD_BARRIER_WAIT() ");
    }
}

void pTHREAD_DETACH( pthread_t thread )
{
    errno = 0;
    if( pthread_detach( thread ) )
    {
        perror("pTHREAD_DETACH() ");
    }
}

void pTHREAD_ATTR_INIT( pthread_attr_t *attr )
{
    errno = 0;
    if( pthread_attr_init( attr ) )
    {
        perror("pTHREAD_ATTR_INIT() ");
    }
}

void pTHREAD_ATTR_DESTROY( pthread_attr_t *attr )
{
    errno = 0;
    if( pthread_attr_destroy( attr ) )
    {
        perror("pTHREAD_ATTR_DESTROY() ");
    }
}

void pTHREAD_ATTR_SETDETACHSTATE( pthread_attr_t *attr, int detachstate )
{
    errno = 0;
    if( pthread_attr_setdetachstate( attr, detachstate ) )
    {
        perror("pTHREAD_ATTR_DESTROY() ");
    }
}

void pTHREAD_MUTEXATTR_SETTYPE( pthread_mutexattr_t *attr, int kind )
{
    errno = 0;
    if( pthread_mutexattr_settype( attr, kind ) )
    {
        perror("pTHREAD_MUTEXATTR_SETTYPE() ");
    }
}

void sem_INIT( sem_t *sem, int pshared, unsigned int value )
{
    errno = 0;
    if( sem_init( sem, pshared, value ) )
    {
        perror("sem_INIT() ");
    }
}

void sem_DESTROY( sem_t *sem )
{
    errno = 0;
    if( sem_destroy( sem ) )
    {
        perror("sem_DESTROY() ");
    }
}

void sem_WAIT( sem_t *sem )
{
    errno = 0;
    if( sem_wait( sem ) )
    {
        perror("sem_WAIT() ");
    }
}

void sem_POST( sem_t *sem )
{
    errno = 0;
    if( sem_post( sem ) )
    {
        perror("sem_POST() ");
    }
}

void sem_GETVALUE( sem_t *sem, int *sval )
{
    errno = 0;
    if( sem_getvalue( sem, sval ) )
    {
        perror("sem_GETVALUE() ");
    }
}


