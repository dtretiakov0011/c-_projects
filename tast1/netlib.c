#include "network.h"

int SOCKET( int domain, int type, int protocol  )
{
    int fd = 0;
    
    if( (fd = socket( domain, type, protocol)) < 0 )
    {
        perror( "error SOCKET():" );
    }
    return fd;
}

int BIND( int sockfd, const struct sockaddr *addr, socklen_t addrlen )
{
    int ret = bind( sockfd, addr, addrlen );
    if( ret != 0 )
    {
        perror( "error BIND():" );
        exit( EXIT_FAILURE );
    }
    return ret;
}

int LINTEN( int sockfd, int backlog )
{
    int ret = listen( sockfd, backlog );
    if( ret != 0 )
    {
        perror( "error LISTEN():" );
    }
    return ret;
}

int ACCEPT( int sockfd, struct sockaddr *addr, socklen_t *addrlen )
{
    int ret = accept( sockfd, addr, addrlen );
    if( ret == -1 )
    {
        perror( "error ACCEPT():" );
    }
    return ret;
}

void INET_PTON( int af, const char *src, void *dst )
{
    if( inet_pton( af, src, dst ) <= 0 )
    {
        perror( "error INET_PTON():" );
    }
}

void CONNECT( int sockfd, const struct sockaddr *addr, socklen_t addrlen )
{
    if( connect( sockfd, addr, addrlen ) != 0 )
    {
        perror( "error CONNECT():" );
    }
}

