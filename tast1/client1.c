#include "network.h"
#include "client1.h"
#include "platform.h"
#include "threads.h"

#define HALF_A_SECOND 500000

pthread_t th[ 20 ];
pthread_t th_index = 0;

uint16_t PORT = 5000;
uint8_t TH_flag = 1;

int main(int argc, char *argv[]) {

    char recvBuff[1024], n = 0;
    int connfd;
    
    //test
    work_client_net();

    return EXIT_SUCCESS;
    //
}


static void work_client_task1( char * const recvBuff )
{
        //prepare request
        char buff[1024];
        memset( buff, 0, 1024 );

        prepere_command( recvBuff, buff );

        //process request
        find_file( buff );

        //result
        memset( recvBuff, 0, 1024 );
        read_res( recvBuff );
}

static int work_client_net( void )
{
    int listenfd = 0, connfd = 0;
    struct sockaddr_in serv_addr;

    char sendBuff[1025];
    time_t ticks;

    listenfd = SOCKET(AF_INET, SOCK_STREAM, 0);
    memset(&serv_addr, '0', sizeof(serv_addr));
    memset(sendBuff, '0', sizeof(sendBuff));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons( PORT );

    BIND(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));

    LINTEN(listenfd, 10);

    while(1) {
        connfd = ACCEPT(listenfd, (struct sockaddr*)NULL, NULL);

//malloc!!!
        int * fd = malloc( sizeof( int ) );
        *fd = connfd;

        pTHREAD_CREATE( &th[ th_index++ ] , NULL, &thread_funct, (void *)fd );

    }
}

static void work_thread( const int * const connfd )
{
    pthread_t th;

    pTHREAD_CREATE( &th, NULL, &thread_funct, (void *)connfd );
}

static void * thread_funct( void * fd )
{
    //for "processing"
        pthread_t th;
        th_s process;
        process.fd = *(int *)fd;
        process.flag = 1;
        pTHREAD_CREATE( &th, NULL, &processing_funct, (void *)&process );

//search file
    int n;
    char recvBuff [ 1024 ];

    //read request
    while( (n = read( *(int *)fd , recvBuff, sizeof(recvBuff) - 1 )) > 0 )
    {
        recvBuff[n] = 0;
        break;
    }   

    work_client_task1( recvBuff );
    process.flag = 0;
    pTHREAD_JOIN( th, NULL );



    //response
    if( strlen( recvBuff ) )
    {
        write( *(int *)fd, recvBuff, strlen( recvBuff ) );
        close( *(int *)fd );
    }
    else
    {
        char * noFile_response = "no such file\n";
        
        write( *(int *)fd, noFile_response, strlen( noFile_response ) );
        close( *(int *)fd );
    }
}

static void * processing_funct( void * fd )
{
    th_s * c_th = (th_s*)fd;
    char * replay = "processing\n";

    while( 1 )
    {
        usleep( HALF_A_SECOND );
        if( c_th->flag )
            write( *(int *)fd, replay, strlen(replay) );
        else
            pthread_exit( fd );
    }
}
