#include "threads.h"

void pTHREAD_CREATE( pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg )
{
    errno = 0;
    if( pthread_create( thread, attr, start_routine, arg ) )
    {
        perror("pTHREAD_CREATE() ");
        exit(1);
    }
}

void pTHREAD_JOIN( pthread_t thread, void **retval )
{
    errno = 0;
    if( pthread_join( thread, retval ) )
    {
        perror("pTHREAD_JOIN() ");
    }
}

void pTHREAD_MUTEX_INIT( pthread_mutex_t *mutex, const pthread_mutexattr_t *mutexattr )
{
    errno = 0;
    if( pthread_mutex_init( mutex, mutexattr ) )
    {
        perror("pTHREAD_INIT() ");
    }
}

void pTHREAD_MUTEX_DESTROY( pthread_mutex_t *mutex )
{
    errno = 0;
    if( pthread_mutex_destroy( mutex ) )
    {
        perror("pTHREAD_MUTEX_DESTROY() ");
    }
}

void pTHREAD_MUTEX_LOCK( pthread_mutex_t *mutex )
{
    errno = 0;
    if( pthread_mutex_lock( mutex ) )
    {
        perror("pTHREAD_MUTEX_LOCK() ");
    }
}

void pTHREAD_MUTEX_UNLOCK( pthread_mutex_t *mutex )
{
    errno = 0;
    if( pthread_mutex_unlock( mutex ) )
    {
        perror("pTHREAD_MUTEX_UNLOCK() ");
    }
}

int pTHREAD_MUTEX_TRYLOCK( pthread_mutex_t *mutex )
{
    errno = 0;
    int ret;
    if( (ret = pthread_mutex_trylock( mutex )) != 0 )
    {
        perror("pTHREAD_MUTEX_TRYLOCK() EBUSY");
    }
    return ret;
}











