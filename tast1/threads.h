#ifndef _THREADS_H_
#define _THREADS_H_

#include <pthread.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>


void pTHREAD_CREATE( pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg );
void pTHREAD_JOIN( pthread_t thread, void **retval );
void pTHREAD_MUTEX_INIT( pthread_mutex_t *mutex, const pthread_mutexattr_t *mutexattr );
void pTHREAD_MUTEX_DESTROY( pthread_mutex_t *mutex );
void pTHREAD_MUTEX_LOCK( pthread_mutex_t *mutex );
void pTHREAD_MUTEX_UNLOCK( pthread_mutex_t *mutex );
int pTHREAD_MUTEX_TRYLOCK( pthread_mutex_t *mutex );

#endif
