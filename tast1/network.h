#ifndef _NETWORK_H_
#define _NETWORK_H_

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h> 


#define LOCALHOST "127.0.0.1\0"


//wrapper
int SOCKET( int domain, int type, int protocol  );
int BIND( int sockfd, const struct sockaddr *addr, socklen_t addrlen );
int LINTEN( int sockfd, int backlog );
int ACCEPT( int sockfd, struct sockaddr *addr, socklen_t *addrlen );
void INET_PTON( int af, const char *src, void *dst );
void CONNECT( int sockfd, const struct sockaddr *addr, socklen_t addrlen );



#endif