#include "network.h"
#include "server1.h"

uint16_t PORT = 5000;

int main(int argc, char *argv[]) {

    while (1)
    {
        char s_file_from_user [1024];
        memset( s_file_from_user, 0, 1024 );
        get_request_from_user( s_file_from_user );

        server_work( s_file_from_user );
    }
     return 0;
}




static void get_request_from_user( char * const s_from_user )
{
    printf( "%s", "Enter file to search:" );
    if( scanf( "%s", s_from_user ) != 1 )
    {
        perror("scanf() error");
    }
    printf( "file name: %s\n", s_from_user );
}

static void server_work( const char * const file_to_search )
{
    int sockfd = 0, n = 0;
    char recvBuff[1024];
    struct sockaddr_in serv_addr;

    memset(recvBuff, '0',sizeof(recvBuff));
    sockfd = SOCKET(AF_INET, SOCK_STREAM, 0);

    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons( PORT );

    INET_PTON(AF_INET, LOCALHOST, &serv_addr.sin_addr);


    CONNECT(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr));

    //test
    write( sockfd, file_to_search, strlen( file_to_search ) );

    while(1)
    {
        while ( (n = read(sockfd, recvBuff, sizeof(recvBuff)-1)) > 0)
        {
            recvBuff[n] = 0;
            write( 1, recvBuff, strlen( recvBuff ) );
            
            if( strcmp( "processing\n", recvBuff ) )
                return;
            else if( ! strcmp( "no such file\n", recvBuff ) )
            {
                write( 1, recvBuff, strlen( recvBuff ) );
                return;
            }
        }
    }

}
