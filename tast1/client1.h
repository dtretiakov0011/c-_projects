#ifndef _CLIENT_H_
#define _CLIENT_H_


typedef struct th_t
{
    int fd;
    int flag;
}              th_s;






static int work_client_net( void );
static void work_client_task1( char * const recvBuff );
static void work_thread( const int * const connfd );
static void * thread_funct( void * args );
static void * processing_funct( void * fd );




#endif



