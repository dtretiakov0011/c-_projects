#include "platform.h"
#include <pthread.h>

char * prepere_command( char * file_name , char * command )
{
    pthread_t         self;
    self = pthread_self();

    char * command1 = "find ~ -maxdepth 10 -name \"";
    char * command2 = "\" -print > ";
    char command_file[ 20 ] = { 0 };
    sprintf( command_file, "%lu", self );
    char * command3 = ".txt 2>/dev/null";

    strcat( command, command1 );
    strcat( command, file_name );
    strcat( command, command2 );
    strcat( command, command_file );
    strcat( command, command3 );

    return command;
}

void find_file( char * command )
{
    int a = system( command );
}

void read_res( char * const res )
{
    int fd, pos = 0;

    char fileName[ 25 ] = { 0 };
    pthread_t         self;
    self = pthread_self();
    
    sprintf( fileName, "%lu", self );
    strcat( fileName, ".txt" );

    fd = open( fileName, O_RDONLY );

    while( (read( fd, &res[pos++], 1 )) != 0 ) { }
    
    close( fd );
    remove( fileName );
}

